#include "aluno.hpp"
#include <iostream> 

using namespace std;

Aluno::Aluno(){
   setNome("");
   setMatricula("");
   setIdade(0);
   setSexo("");
   setTelefone("");
   setIra(5.0);
   setSemestre(1);
   setCurso("Engenharias");
}
Aluno::~Aluno(){}

void Aluno::setIra(float ira){
   this->ira = ira;
}
float Aluno::getIra(){
   return ira;
}
void Aluno::setSemestre(int semestre){
   this->semestre = semestre;
}
int Aluno::getSemestre(){
   return semestre;
}
void Aluno::setCurso(string curso){
   this->curso = curso;
}
string Aluno::getCurso(){
   return curso;
}

void Aluno::imprimeDadosAluno(){
   cout << "Dados do Aluno" << endl;
   imprimeDados();
   cout << "Ira: " << getIra() << endl;
   cout << "Semestre: " << getSemestre() << endl;
   cout << "Curso: " << getCurso() << endl << endl;
}






