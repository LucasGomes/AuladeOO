#include "pessoa.hpp"
#include "professor.hpp"
#include <string>

Professor::Professor(){
   setNome("");
   setMatricula("");
   setIdade("");
   setSexo("");
   setTelefone("");
   setFormacao("");
   setSalario("");
   setSala("");
}

Professor::~Professor();

string Professor::getFormacao(){
   return formacao;
}

void Professor::setFormacao(string formacao){
   this->formacao = formacao;
}

string Professor::getSalario(){
   return salario;
}

void Professor::setSalario(string salario){
   this->salario = salario;
}

int Professor::getSala(){
   return sala;
}

