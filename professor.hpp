#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP
#include "pessoa.hpp"
#include <string>

using namespace std;

class Professor : public Pessoa {

private:
  string formacao, salario;
  int sala;


public:
  Professor();
  ~Professor();
  string getFormacao();
  void setFormacao(string formacao);
  string getSalario();
  void setSalario(string salario);
  int getSala();
  void setSala(int sala);


};

#endif
